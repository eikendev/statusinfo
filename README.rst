.. image:: https://img.shields.io/pypi/status/statusinfo.svg
    :target: https://pypi.org/project/statusinfo/

.. image:: https://img.shields.io/pypi/l/statusinfo.svg
    :target: https://pypi.org/project/statusinfo/

.. image:: https://img.shields.io/pypi/pyversions/statusinfo.svg
    :target: https://pypi.org/project/statusinfo/

.. image:: https://img.shields.io/pypi/v/statusinfo.svg
    :target: https://pypi.org/project/statusinfo/

.. image:: https://img.shields.io/pypi/dm/statusinfo.svg
    :target: https://pypi.org/project/statusinfo/

Usage
=====

A tool for gathering status information.

Installation
============

From PyPI
---------

.. code:: bash

    pip install statusinfo

From Source
-----------

.. code:: bash

    ./setup.py install

Fedora
------

.. code:: bash

    sudo dnf copr enable eikendev/statusinfo
    sudo dnf install python3-statusinfo

Development
===========

The source code is located on `GitLab <https://gitlab.com/eikendev/statusinfo>`_.
To check out the repository, the following command can be used.

.. code:: bash

    git clone https://gitlab.com/eikendev/statusinfo.git
